BEGIN {
RS="(\r\n|\n)";
FS="\t";
xmap["#Hour#"] = "#Hour#"
xmap["#Program#"] = "#Program#"
xmap["#Channel#"] = "#Channel#"
xmap["#Time#"] = "#Time#"
xmap["#Degrees#"] = "#Degrees#"
xmap["#Percentage#"] = "#Percentage#"
xmap["#Pre-formula#"] = "#Pre-formula#"
xmap["#Post-formula#"] = "#Post-formula#"
xmap["#Formula#"] = "#Formula#"
xmap["#Ending#"] = "#Ending#"

}

BEGINFILE {
	print "Checking "FILENAME"...\n"
	err=0;
	warn=0;
}

{
	k=match($0,"^;");
	if (k>0)
	{
		next;
	}
	switch(NF)
	{
	case 5:
		k=match($1,"[0-9]+");
		if (k==0)
		{
			print "Error in line "FNR"\nInvalid frame_id ["$1"]";
			err++;
			id=99999999;
		}
		else
		{
			id=int($1);
		}
		if (IDS[$1]!="")
		{
			print "Error in line "FNR"\nDupplicated frame_id number ["$1"], also in line "IDS[$1];
			err++;
		}
		else
		{
			IDS[$1]=NR;
		}
		k=match($2,"#[A-Z]+#([[:space:]]#[A-Z]+#)*");
		if (k==0)
		{
			print "Error in line "FNR"\nInvalid entity list specification ["$2"] at field 2";
			err++;
		}
		s2=$2;
		n2=gsub("#[A-Z]+#","",s2);
		k=match($3,"#[^#]+#([[:space:]]#[^#]+#)*");
		if (k==0)
		{
			print "Error in line "FNR"\nInvalid entity list mapping specification ["$3"] at field 3";
			err++;
		}
		else
		{
			s3=$3;
			n3=gsub("#[^#]+#","",s3);
			if (n2!=n3)
			{
				print "Error in line "FNR"\nEntity list vs. Entity list mapping items mismatch ("n2" vs. "n3")";
				err++;
			}
			else
			{
				delete maps;
				for (xx in xmap)
				{
					maps[xx]=xmap[xx];
				}
				s3=$3;
				k2=match(s3,"#[^#]+#")
				while (k2>0)
				{
					s=substr(s3,RSTART,RLENGTH);
					maps[s]=s;
					sub("#[^#]+#","",s3);
					k2=match(s3,"#[^#]+#")
				}
			}
		}
		k=match($4,"[[:alnum:]_-]+");
		if (k==0)
		{
			print "Error in line "FNR"\nInvalid or badly formed intent name specification ["$4"] at field 4";
			err++;
		}
		k=match($5,"[[:alnum:]_-]+");
		if (k==0)
		{
			print "Error in line "FNR"\nInvalid or badly formed extra field specification ["$5"] at field 5";
			err++;
		}
		break;
	case 3:
		k=match($1,"([[:alnum:]_-]+)(,[[:alnum:]_-]+)*");
		if (k==0)
		{
			print "Error in line "FNR"\nInvalid or badly formed formula field specification ["$1"] at field 1";
			err++;
		}
		k=match($2,"[[:alnum:]_-]+");
		if (k==0)
		{
			print "Error in line "FNR"\nInvalid or badly formed model type specification ["$2"] at field 2";
			err++;
		}
		m=split($3,A," [+] ");
		if (m==0)
		{
			print "Error in line "FNR"\nInvalid or badly formed generation specification ["$3"] at field 3";
			err++;
		}
		for (i=1; i<=m; i++)
		{
			w=split(A[i],B,"[.]");
			if (w==0)
			{
				print "Error in line "FNR"\nInvalid or badly formed generation specification ["A[i]"] at field 3";
				err++;
			}
			for (j=1; j<=w; j++)
			{
				x=split(B[j],C,"=");
				if (x!=2)
				{
					if (x>2)
					{
						xerr=0;
						for (q=2; q<x; q++)
						{
							k2=match(C[q],"[^,]+$");
							if (k2>0)
							{
								s=substr(C[q],RSTART,RLENGTH);
								print "Error in line "FNR"\nPossible missing \".\" near \""s"\" at key/value pair ["B[j]"] in field 3";
								xerr++;
							}
						}
						if (xerr==0)
						{
							print "Error in line "FNR"\nPossible missing \".\"s at key/value pair ["B[j]"] in field 3";
							xerr=1;
						}
					}
					else
					{
						print "Error in line "FNR"\nPossible missing \"=\" at key/value pair ["B[j]"] in field 2";
						xerr=1;
					}
					err+=xerr;
				}
				else
				{
					k=match(C[1],"^[[:alnum:]_-]+$");
					if (k==0)
					{
						k=match(C[1],"(^[[:space:]]+|[[:space:]]+$)");
						if (k>0)
						{
							print "Error in line "FNR"\nExtra spaces not allowed in key ["C[1]"] at block ["B[j]"] in field 3";
						}
						else
						{
							print "Error in line "FNR"\nInvalid or badly formed key ["C[1]"] at block ["B[j]"] in field 3";
						}
						err++;
					}
					k=match(C[2],"^[^.]+$");
					if (k==0)
					{
						print "Error in line "FNR"\nInvalid or badly formed value ["C[2]"] at block ["B[j]"] in field 3";
						err++;
					}
					else if (C[1]!="const")
					{
						k=match(C[2],"(^[[:space:]]+|[[:space:]]+$)");
						if (k>0)
						{
							print "Error in line "FNR"\nExtra spaces not allowed in value ["C[2]"] at block ["B[j]"] in field 3";
							err++;
						}
					}
					if (id>=0)
					{
					if ((k2=match(C[2],"^#[^#]+#$"))>0)
					{
						if (maps[C[2]]=="")
						{
							print "Error in line "FNR"\nEntity type ["C[2]"] specified at block ["B[j]"] in field 3 is not defined";
							err++;
						}
					}
					}
				}
			}
		}
		break;
	case 2:
		warn++;
		k=match($1,"([[:alnum:]_-]+)(,[[:alnum:]_-]+)*");
		if (k==0)
		{
			print "Error in line "FNR"\nInvalid or badly formed formula field specification ["$1"] at field 1";
			err++;
		}
		m=split($2,A," [+] ");
		if (m==0)
		{
			print "Error in line "FNR"\nInvalid or badly formed generation specification ["$2"] at field 2";
			err++;
		}
		for (i=1; i<=m; i++)
		{
			w=split(A[i],B,"[.]");
			if (w==0)
			{
				print "Error in line "FNR"\nInvalid or badly formed generation specification ["A[i]"] at field 2";
				err++;
			}
			for (j=1; j<=w; j++)
			{
				x=split(B[j],C,"=");
				if (x!=2)
				{
					if (x>2)
					{
						xerr=0;
						for (q=2; q<=x; q++)
						{
							k2=match(C[q],"[^,]+");
							if (k2>0)
							{
								s=substr(C[q],RSTART,RLENGTH);
								print "Error in line "FNR"\nPossible missing \".\" near \"s\" at key/value pair ["B[j]"] in field 3";
								xerr++;
								break;
							}
						}
						if (xerr==0)
						{
							print "Error in line "FNR"\nPossible missing \".\"s at key/value pair ["B[j]"] in field 3";
						}
					}
					else
					{
						print "Error in line "FNR"\nPossible missing \"=\" at key/value pair ["B[j]"] in field 2";
					}
					err++;
				}
				else
				{
					k=match(C[1],"^[[:alnum:]_-]+$");
					if (k==0)
					{
						k=match(C[1],"(^[[:space:]]+|[[:space:]]+$)");
						if (k>0)
						{
							print "Error in line "FNR"\nExtra spaces in key ["C[1]"] at block ["B[j]"] in field 2";
						}
						else
						{
							print "Error in line "FNR"\nInvalid or badly formed key ["C[1]"] at block ["B[j]"] in field 2";
						}
						err++;
					}
					k=match(C[2],"^[^.]+$");
					if (k==0)
					{
						print "Error in line "FNR"\nInvalid or badly formed value ["C[2]"] at block ["B[j]"] in field 2";
						err++;
					}
					else if (C[1]!="const")
					{
						k=match(C[2],"(^[[:space:]]+|[[:space:]]+$)");
						if (k>0)
						{
							print "Error in line "FNR"\nExtra spaces not allowed in value ["C[2]"] at block ["B[j]"] in field 3";
							err++;
						}
					}
					if (id>=0)
					{
					if ((k2=match(C[2],"^#[^#]+#$"))>0)
					{
						if (maps[C[2]]=="")
						{
							print "Error in line "FNR"\nEntity type ["C[2]"] specified at block ["B[j]"] in field 3 is not defined";
							err++;
						}
					}
					}
				}
			}
		}
		break;
	default:
		print "Error in line "FNR"\nFormat not supported with ("NF" fields)";
		err++;
	}
}

ENDFILE {
	if (err>0)
	{
		print "\n"FILENAME" checking finished with "err" ERRORS detected!!!"
	}
	else
	{
		print "\n"FILENAME" is OK."
	}
	if (warn>0)
	{
		print "\nWARNING!!!\nFile ["FILENAME"] has an old format with just 2 fields in the frame definition lines that is to be deprecated!!!\n";
	}
	else
	{
		print "\n";
	}
}
