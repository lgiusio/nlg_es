BEGIN {
RS="(\r\n|\n)";
OFS=FS="\t";
xmap["#Hour#"] = "#Hour#"
xmap["#Program#"] = "#Program#"
xmap["#Channel#"] = "#Channel#"
xmap["#Time#"] = "#Time#"
xmap["#Degrees#"] = "#Degrees#"
xmap["#Percentage#"] = "#Percentage#"
xmap["#Pre-formula#"] = "#Pre-formula#"
xmap["#Post-formula#"] = "#Post-formula#"
xmap["#Formula#"] = "#Formula#"
xmap["#Ending#"] = "#Ending#"
curfile="";
}

function rewind()
{
    # shift remaining arguments up
    for (i = ARGC; i > ARGIND; i--)
        ARGV[i] = ARGV[i-1];

    # make sure gawk knows to keep going
    ARGC++;

    # make current file next to get done
    ARGV[ARGIND+1] = FILENAME;

    # do it
#    nextfile;
}


BEGINFILE {
	if (curfile!=FILENAME)
	{
		needrewind=1;
		print "Compiling "FILENAME"...\n" >"/dev/tty";
		print "Reading BLOCKS and MULTIBLOCKS...\n" >"/dev/tty";
		curfile=FILENAME;
		x=split(curfile,FF,"[.]");
		curname=FF[1];
		for (i=2;i<x;i++)
		{
			curname=curname"."FF[i];
		}
		curname=curname"_obj."FF[x];
		err=0;
		warn=0;
	}
	else
	{
		needrewind=0;
		err=0;
		warn=0;
		print "Generating output...\n" >"/dev/tty";
	}
}

{
	k=match($0,"^;");
	if (k>0)
	{
		next;
	}
	switch(NF)
	{
	case 5:
		k=match($1,"[0-9]+");
		if (k==0)
		{
			print "Error in line "FNR"\nInvalid frame_id ["$1"]";
			err++;
			id=99999999;
		}
		else
		{
			id=int($1);
		}
		if (IDS[$1]!="")
		{
			print "Error in line "FNR"\nDupplicated frame_id number ["$1"], also in line "IDS[$1];
			err++;
		}
		else
		{
			IDS[$1]=NR;
		}
		k=match($2,"#[A-Z]+#([[:space:]]#[A-Z]+#)*");
		if (k==0)
		{
			print "Error in line "FNR"\nInvalid entity list specification ["$2"] at field 2";
			err++;
		}
		s2=$2;
		n2=gsub("#[A-Z]+#","",s2);
		k=match($3,"#[^#]+#([[:space:]]#[^#]+#)*");
		if (k==0)
		{
			print "Error in line "FNR"\nInvalid entity list mapping specification ["$3"] at field 3";
			err++;
		}
		else
		{
			s3=$3;
			n3=gsub("#[^#]+#","",s3);
			if (n2!=n3)
			{
				print "Error in line "FNR"\nEntity list vs. Entity list mapping items mismatch ("n2" vs. "n3")";
				err++;
			}
			else
			{
				delete maps;
				for (xx in xmap)
				{
					maps[xx]=xmap[xx];
				}
				s3=$3;
				k2=match(s3,"#[^#]+#")
				while (k2>0)
				{
					s=substr(s3,RSTART,RLENGTH);
					maps[s]=s;
					sub("#[^#]+#","",s3);
					k2=match(s3,"#[^#]+#")
				}
			}
		}
		k=match($4,"[[:alnum:]_-]+");
		if (k==0)
		{
			print "Error in line "FNR"\nInvalid or badly formed intent name specification ["$4"] at field 4";
			err++;
		}
		k=match($5,"[[:alnum:]_-]+");
		if (k==0)
		{
			print "Error in line "FNR"\nInvalid or badly formed extra field specification ["$5"] at field 5";
			err++;
		}
		
		if (err==0)
		{
			if (id==-10)
			{
				read_blocks=1;
				read_multiblkocks=0;
			}
			else if (id<-100)
			{
				read_blocks=0;
				read_multiblkocks=1;
			}
			else
			{
				read_blocks=0;
				read_multiblkocks=0;
			}
		}
		if (needrewind==0)
		{
			if (read_blocks==0)
			{
				print $0 >curname;
			}
		}
		break;
	case 3:
		k=match($1,"([[:alnum:]_-]+)(,[[:alnum:]_-]+)*");
		if (k==0)
		{
			print "Error in line "FNR"\nInvalid or badly formed formula field specification ["$1"] at field 1";
			err++;
		}
		if (id!=-10)
		{
			k=match($2,"([0BINMPS]+)_([0-9]+)(:([[:alnum:]_-]+))?");
		}
		else
		{
			k=match($2,"[[:alnum:]_-]+");
		}
		if (k==0)
		{
			print "Error in line "FNR"\nInvalid or badly formed model type specification ["$2"] at field 2";
			err++;
		}
		m=split($3,A," [+] ");
		if (m==0)
		{
			print "Error in line "FNR"\nInvalid or badly formed generation specification ["$3"] at field 3";
			err++;
		}
		for (i=1; i<=m; i++)
		{
			sub("^[[]","",A[i]);
			sub("^[]]","",A[i]);
			sub("[[]$","",A[i]);
			sub("[]]$","",A[i]);
			w=split(A[i],B,"[.]");
			if (w==0)
			{
				print "Error in line "FNR"\nInvalid or badly formed generation specification ["A[i]"] at field 3";
				err++;
			}
			for (j=1; j<=w; j++)
			{
				x=split(B[j],C,"=");
				if (x!=2)
				{
					if (x>2)
					{
						xerr=0;
						for (q=2; q<x; q++)
						{
							k2=match(C[q],"[^,]+$");
							if (k2>0)
							{
								s=substr(C[q],RSTART,RLENGTH);
								print "Error in line "FNR"\nPossible missing \".\" near \""s"\" at key/value pair ["B[j]"] in field 3";
								xerr++;
							}
						}
						if (xerr==0)
						{
							print "Error in line "FNR"\nPossible missing \".\"s at key/value pair ["B[j]"] in field 3";
							xerr=1;
						}
					}
					else
					{
						print "Error in line "FNR"\nPossible missing \"=\" at key/value pair ["B[j]"] in field 2";
						xerr=1;
					}
					err+=xerr;
				}
				else
				{
					k=match(C[1],"^[[:alnum:]_-]+$");
					if (k==0)
					{
						k=match(C[1],"(^[[:space:]]+|[[:space:]]+$)");
						if (k>0)
						{
							print "Error in line "FNR"\nExtra spaces not allowed in key ["C[1]"] at block ["B[j]"] in field 3";
						}
						else
						{
							print "Error in line "FNR"\nInvalid or badly formed key ["C[1]"] at block ["B[j]"] in field 3";
						}
						err++;
					}
					k=match(C[2],"^[^.]+$");
					if (k==0)
					{
						print "Error in line "FNR"\nInvalid or badly formed value ["C[2]"] at block ["B[j]"] in field 3";
						err++;
					}
					else if (C[1]!="const")
					{
						k=match(C[2],"(^[[:space:]]+|[[:space:]]+$)");
						if (k>0)
						{
							print "Error in line "FNR"\nExtra spaces not allowed in value ["C[2]"] at block ["B[j]"] in field 3";
							err++;
						}
					}
					if (id>=0)
					{
					if ((k2=match(C[2],"^#[^#]+#$"))>0)
					{
						if (maps[C[2]]=="")
						{
							print "Error in line "FNR"\nEntity type ["C[2]"] specified at block ["B[j]"] in field 3 is not defined";
							err++;
						}
					}
					}
				}
			}
		}
		
		if (err==0)
		{
			if (needrewind==1)
			{
				if (read_blocks==1)
				{
					if (block[$1]!="")
					{
						print "Error in line "FNR"\nDupplicated block definition for block "$1;
						err++;
					}
					else
					{
						k=match($3,"[[][^]]+[]]");
						if (k>0)
						{
							print "Error in line "FNR"\nBlock ["$1"]. Blocks cannot contain optional elements "$3;
							err++;
						}
						else
						{
							block[$1]=$3;
						}
					}
				}
			}
			else
			{
				if ((read_blocks==1))
				{
					next;
				}
				else
				{
					s3=$3;
					k=match(s3,"\\<block=[[:alnum:]_-]+");
					while (k>0)
					{
						bname=substr(s3,RSTART+6,RLENGTH-6);
						if (block[bname]=="")
						{
							print "Error in line "FNR"\nBlock ["bname"]. Block is empty or is not defined";
							err++;
							org="\\<block="bname;
							gsub(org,"ERROR",s3);
							k=match(s3,"\\<block=[[:alnum:]_-]+");
						}
						else
						{
							org="\\<block="bname;
							gsub(org,block[bname],s3);
							k=match(s3,"\\<block=[[:alnum:]_-]+");
						}
					}
					$3=s3;
				}
			}
		}
		if (err==0)
		{
			if (needrewind==0)
			{
				if (read_blocks==0)
				{
					s3=$3;
					nf=patsplit(s3,A,"[[][^]]+[]]",S);
					if (nf==0)
					{
						nb=1;
						B[nb]=s3;
						C[nb]=1;
						nb++;
					}
					else
					{
						nb=1;
						if (S[0]!="")
						{
							B[nb]=S[0];
							C[nb]=1;
							nb++;
						}
						for (i=1; i<nf; i++)
						{
							B[nb]=A[i];
							sub("^[[]","",B[nb]);
							sub("[]]$","",B[nb]);
							C[nb]=0;
							nb++;
							B[nb]=S[i];
							C[nb]=1;
							nb++;
						}
						B[nb]=A[nf];
						sub("^[[]","",B[nb]);
						sub("[]]$","",B[nb]);
						C[nb]=0;
						nb++;
						if (S[nf]!="")
						{
							B[nb]=S[nf];
							C[nb]=1;
							nb++;
						}
					}
					nb--;
					mf=2^nb;
					for (i=0; i<mf; i++)
					{
						xi=i;
						s0="";
						for (j=1; j<=nb; j++)
						{
							xj=xi%2;
							if ((C[j]==1) || (xj==1))
							{
								s0=s0""B[j];
							}
							xi=xi/2;
						}
						if (s0!="")
						{
							sub("^ [+] ","",s0);
							sub(" [+] $","",s0);
							sub("^[+] ","",s0);
							sub(" [+]$","",s0);
							$3=s0;
							if (X[$0]=="")
							{
								print $0 >curname;
								X[$0]="1";
							}
						}
					}
					delete B;
					delete C;
					delete S;
					delete A;
					delete X;
				}
			}
		}
		break;
	case 2:
		warn++;
		k=match($1,"([[:alnum:]_-]+)(,[[:alnum:]_-]+)*");
		if (k==0)
		{
			print "Error in line "FNR"\nInvalid or badly formed formula field specification ["$1"] at field 1";
			err++;
		}
		m=split($2,A," [+] ");
		if (m==0)
		{
			print "Error in line "FNR"\nInvalid or badly formed generation specification ["$2"] at field 2";
			err++;
		}
		for (i=1; i<=m; i++)
		{
			w=split(A[i],B,"[.]");
			if (w==0)
			{
				print "Error in line "FNR"\nInvalid or badly formed generation specification ["A[i]"] at field 2";
				err++;
			}
			for (j=1; j<=w; j++)
			{
				x=split(B[j],C,"=");
				if (x!=2)
				{
					if (x>2)
					{
						xerr=0;
						for (q=2; q<=x; q++)
						{
							k2=match(C[q],"[^,]+");
							if (k2>0)
							{
								s=substr(C[q],RSTART,RLENGTH);
								print "Error in line "FNR"\nPossible missing \".\" near \"s\" at key/value pair ["B[j]"] in field 3";
								xerr++;
								break;
							}
						}
						if (xerr==0)
						{
							print "Error in line "FNR"\nPossible missing \".\"s at key/value pair ["B[j]"] in field 3";
						}
					}
					else
					{
						print "Error in line "FNR"\nPossible missing \"=\" at key/value pair ["B[j]"] in field 2";
					}
					err++;
				}
				else
				{
					k=match(C[1],"^[[:alnum:]_-]+$");
					if (k==0)
					{
						k=match(C[1],"(^[[:space:]]+|[[:space:]]+$)");
						if (k>0)
						{
							print "Error in line "FNR"\nExtra spaces in key ["C[1]"] at block ["B[j]"] in field 2";
						}
						else
						{
							print "Error in line "FNR"\nInvalid or badly formed key ["C[1]"] at block ["B[j]"] in field 2";
						}
						err++;
					}
					k=match(C[2],"^[^.]+$");
					if (k==0)
					{
						print "Error in line "FNR"\nInvalid or badly formed value ["C[2]"] at block ["B[j]"] in field 2";
						err++;
					}
					else if (C[1]!="const")
					{
						k=match(C[2],"(^[[:space:]]+|[[:space:]]+$)");
						if (k>0)
						{
							print "Error in line "FNR"\nExtra spaces not allowed in value ["C[2]"] at block ["B[j]"] in field 3";
							err++;
						}
					}
					if (id>=0)
					{
					if ((k2=match(C[2],"^#[^#]+#$"))>0)
					{
						if (maps[C[2]]=="")
						{
							print "Error in line "FNR"\nEntity type ["C[2]"] specified at block ["B[j]"] in field 3 is not defined";
							err++;
						}
					}
					}
				}
			}
		}
		break;
	case 1:
		k=match($0,"^MULTIBLOCK=-[0-9]+$");
		if (k>0)
		{
			s=substr($0,RSTART+11,RLENGTH-11);
			k2=match($0,"^MULTIBLOCK=-[1-9][0-9][0-9]+$");
			if (k2==0)
			{
				print "Error in line "FNR"\nInvalid MULTIBKLOCK reference ("s")";
				err++;
			}
		}
		else
		{
			print "Error in line "FNR"\nFormat not supported with ("NF" fields). MULTIBLOCK reference expected";
			err++;
		}
		if (err==0)
		{
			if (needrewind==0)
			{
				if (read_blocks==0)
				{
					print $0 >curname;
				}
			}
		}
		break;
	default:
		print "Error in line "FNR"\nFormat not supported with ("NF" fields)";
		err++;
	}
}

ENDFILE {
	if (err>0)
	{
		if (needrewind==1)
		{
			delete IDS;
		}
		else
		{
			delete block;
		}
		print "\n"FILENAME" checking finished with "err" ERRORS detected!!!"
	}
	else
	{
		if (needrewind==1)
		{
			stillblocks=1;
			while (stillblocks>0)
			{
				stillblocks=0;
				for (x in block)
				{
					s3=block[x];
					k=match(s3,"\\<block=[[:alnum:]_-]+");
					while (k>0)
					{
						stillblocks++;
						bname=substr(s3,RSTART+6,RLENGTH-6);
						org="\\<block="bname;
						gsub(org,block[bname],s3);
						k=match(s3,"block=[[:alnum:]_-]+");
					}
					block[x]=s3;
				}
			}
			delete IDS;
			rewind();
		}
		else
		{
			delete block;
		}
		print "\n"FILENAME" is OK."
	}
	if (warn>0)
	{
		print "\nWARNING!!!\nFile ["FILENAME"] has an old format with just 2 fields in the frame definition lines that is to be deprecated!!!\n";
	}
	else
	{
		print "\n";
	}
}
