import json
import re
import csv
import subprocess
import os
import sys
import ctypes
import postprocess.btxlex as btxlex

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

SENTENCE_TYPES_LIST = ['B','I','M','N','P','S']

def printf(format, *args):
    sys.stdout.write(format % args)

def is_not_blank(my_string):
    return bool(my_string and my_string.strip())

##### PRODUCE A PLAIN LIST OF SENTENECES
#####   One sentence per line
#####   Enclosed among doble quotes ""
#####!!!WARNING!!! Internal double quotes not escaped!!!!
#####
def string_list_to_plain_list_with_ID(utterances, f):
    tag_regex=re.compile(":#[^#]+#:")
#    json = ""
    for i in range (0, len(utterances)):
        x=utterances[i].split("\t")
        if (x[1]!="ENTITY") and x[1]!="INTENT":
#            json = json + "\"" + tag_regex.sub("",x[1]) + "\"\n"
            f.write(x[0] + "\t" + x[1] + "\t" + "\"" + tag_regex.sub("",x[2]) + "\"\n")
#    return json
    return


##### PRODUCE A PLAIN LIST OF SENTENCES
#####   One sentence per line
#####   Enclosed among doble quotes ""
#####!!!WARNING!!! Internal double quotes not escaped!!!!
#####
def string_list_to_plain_list(utterances):
    spcrem = re.compile(r"(^ +| +$)")
    tag_regex=re.compile(":#[^#]+#:")
    json = []
    for i in range (0, len(utterances)):
        x=utterances[i].split("\t")
        if (x[1]!="ENTITY") and x[1]!="INTENT":
            x = tag_regex.sub("",x[2])
#####!!!WARNING!!! GNAPA a/an before non-vowel/vowel for English!!!!
            m=re.search(r"(\ban [^aeiouAEIOU]|\ba [aeiouAEIOU])",x)
            if m is None:
                x=re.sub("^, ","",x)
                x=x.replace(" '","'")
                x=x.replace(" ,",",")
                x=x.replace(" ;",";")
                x=x.replace(" .",".")
                x=x.replace(" ?","?")
                x=x.replace(" !","!")
                x=x.replace("¿ ","¿")
                x=x.replace("¡ ","¡")
                x=x.replace("s's","s'")
                x=spcrem.sub("",x)
                json.append(x)
    return json

##### PRODUCE A PLAIN LIST OF SENTENCES
#####   One sentence per line
#####   Enclosed among doble quotes ""
#####!!!WARNING!!! Internal double quotes not escaped!!!!
#####
def string_list_to_plain_list_str_with_label(utterances):
    spcrem = re.compile(r"(^ +| +$)")
    tag_regex=re.compile(":#[^#]+#:")
    json = []
    for i in range (0, len(utterances)):
        z=utterances[i].split("\t")
        if (z[1]!="ENTITY") and z[1]!="INTENT":
            x = "\"" + tag_regex.sub("",z[2]) + "\"\n"
#####!!!WARNING!!! GNAPA a/an before non-vowel/vowel for English!!!!
            m=re.search(r"(\ban [^aeiouAEIOU]|\ba [aeiouAEIOU])",x)
            if m is None:
                x=re.sub("^, ","",x)
                x=x.replace(" '","'")
                x=x.replace(" ,",",")
                x=x.replace(" ;",";")
                x=x.replace(" .",".")
                x=x.replace(" ?","?")
                x=x.replace(" !","!")
                x=x.replace("¿ ","¿")
                x=x.replace("¡ ","¡")
                x=x.replace("s's","s'")
                x=spcrem.sub("",x)
                x="\""+z[0]+"\"\t"+x
                json.append(x)
    outstr="".join(json)
    json=[]
    return outstr


##### PRODUCE A PLAIN LIST OF SENTENCES
#####   One sentence per line
#####   Enclosed among doble quotes ""
#####!!!WARNING!!! Internal double quotes not escaped!!!!
#####
def string_list_to_plain_list_str(utterances):
    spcrem = re.compile(r"(^ +| +$)")
    tag_regex=re.compile(":#[^#]+#:")
    json = []
    for i in range (0, len(utterances)):
        x=utterances[i].split("\t")
        if (x[1]!="ENTITY") and x[1]!="INTENT":
            x = "\"" + tag_regex.sub("",x[2]) + "\"\n"
#####!!!WARNING!!! GNAPA a/an before non-vowel/vowel for English!!!!
            m=re.search(r"(\ban [^aeiouAEIOU]|\ba [aeiouAEIOU])",x)
            if m is None:
                x=re.sub("^, ","",x)
                x=x.replace(" '","'")
                x=x.replace(" ,",",")
                x=x.replace(" ;",";")
                x=x.replace(" .",".")
                x=x.replace(" ?","?")
                x=x.replace(" !","!")
                x=x.replace("¿ ","¿")
                x=x.replace("¡ ","¡")
                x=x.replace("s's","s'")
                x=spcrem.sub("",x)
                json.append(x)
    outstr="".join(json)
    json=[]
    return outstr

##### PRODUCE A COMPLETE SET OF INTENTS/ENTITIES/UTTERANCES IN MS-LUIS FORMAT
#####   INTENTS and ENTITIES summarized first (as requested by MS-LUIS format)
#####   One utterance labelling block per original sentence
#####
def string_list_to_MS_LUIS_str(botname, botdescription, botculture, utterances):
    json = []
    json.append("{\n")
    json.append("\t\"name\": \"" + botname.replace("\"","\\\"") + "\",\n")
    json.append("\t\"desc\": \"" + botdescription.replace("\"","\\\"") +"\",\n")
    json.append("\t\"culture\": \"" +botculture.replace("\"","\\\"") + "\",\n")
    json.append("\t\"intents\": [\n")
    numlist=0
    for i in range (0, len(utterances)):
        x=utterances[i].split("\t")
        if (x[1]=="INTENT"):
            if numlist>0:
                json.append(",\n")
            numlist = numlist + 1
            json.append("\t\t{\n")
            json.append("\t\t\t\"Name\": \"" + x[2].replace("\"","\\\"") + "\"\n")
            json.append("\t\t}")
            intent_name=x[2] #GNAPA!!!
    json.append("\n\t],\n")
    json.append("\t\"entities\": [\n")
    numlist=0
    for i in range (0, len(utterances)):
        x=utterances[i].split("\t")
        if (x[1]=="ENTITY"):
            if numlist>0:
                json.append(",\n")
            numlist = numlist + 1
            json.append("\t\t{\n")
            json.append("\t\t\t\"Name\": \"" + x[2].replace("\"","\\\"") + "\"\n")
            json.append("\t\t}")
    json.append("\n\t],\n")
    json.append("\t\"bing_entities\": [],\n")
    json.append("\t\"actions\": [],\n")
    json.append("\t\"model_features\": [],\n")
    json.append("\t\"regex_features\": [],\n")
    json.append("\t\"utterances\": [\n")
    tag_regex=re.compile(":#[^#]+#:")
    numlist=0
    for i in range (0, len(utterances)):
        x=utterances[i].split("\t")
        if (x[1]!="ENTITY") and x[1]!="INTENT":
            if numlist>0:
                json.append(",\n")
            numlist = numlist + 1
            json.append("\t\t{\n")
            json.append("\t\t\t\"text\": \"" + tag_regex.sub("",x[2]).replace("\"","\\\"") + "\",\n")
            json.append("\t\t\t\"intent\": \"" + intent_name.replace("\"","\\\"") + "\",\n")
            m=tag_regex.search(x[2])
            numlist2=0
            while m is not None:
                if numlist2==0:
                    json.append("\t\t\t\"entities\": [\n")
                if numlist2>0:
                    json.append(",\n")
                numlist2 = numlist2 +1
                start=m.start(0)
                tag=m.group(0)
                tag=tag.replace("#","")
                tag=tag.replace(":","")
                x[2] = tag_regex.sub("", x[2], 1)
                m=tag_regex.search(x[2])
                end=m.start(0)
                x[2] = tag_regex.sub("", x[2], 1)
                value = x[2][start:end]
                m=tag_regex.search(x[2])
                json.append("\t\t\t\t{\n")
                json.append("\t\t\t\t\t\"entity\": \"" + tag.replace("\"","\\\"") + "\",\n")
                json.append("\t\t\t\t\t\"_entity_text\": \"" + value.replace("\"","\\\"") + "\",\n")
                json.append("\t\t\t\t\t\"startPOS\": " + str(start) + ",\n")
                json.append("\t\t\t\t\t\"endPOS\": " + str(end-1) + "\n")
                json.append("\t\t\t\t}")
            json.append("\n\t\t\t]\n")
            json.append("\t\t}")
    json.append("\n\t]\n")
    json.append("}\n")
    outval="".join(json)
    json=[]
    return outval


##### PRODUCE A PLAIN LIST OF SENTENECES
#####   One sentence per line
#####   Enclosed among doble quotes ""
#####!!!WARNING!!! Internal double quotes not escaped!!!!
#####
def string_list_to_plain_list_file(utterances, f):
    tag_regex=re.compile(":#[^#]+#:")
#    json = ""
    for i in range (0, len(utterances)):
        x=utterances[i].split("\t")
        if (x[1]!="ENTITY") and x[1]!="INTENT":
#            json = json + "\"" + tag_regex.sub("",x[1]) + "\"\n"
            f.write("\"" + tag_regex.sub("",x[2]) + "\"\n")
#    return json
    return

##### PRODUCE A COMPLETE SET OF INTENTS/ENTITIES/UTTERANCES IN MS-LUIS FORMAT
#####   INTENTS and ENTITIES summarized first (as requested by MS-LUIS format)
#####   One utterance labelling block per original sentence
#####
def string_list_to_MS_LUIS_file(botname, botdescription, botculture, utterances, f):
#    json = "{\n"
#    json = json + "\t\"name\": \"" + botname.replace("\"","\\\"") + "\",\n"
#    json = json + "\t\"desc\": \"" + botdescription.replace("\"","\\\"") +"\",\n"
#    json = json + "\t\"culture\": \"" +botculture.replace("\"","\\\"") + "\",\n"
#    json = json + "\t\"intents\": [\n"
    f.write("{\n")
    f.write("\t\"name\": \"" + botname.replace("\"","\\\"") + "\",\n")
    f.write("\t\"desc\": \"" + botdescription.replace("\"","\\\"") +"\",\n")
    f.write("\t\"culture\": \"" +botculture.replace("\"","\\\"") + "\",\n")
    f.write("\t\"intents\": [\n")
    numlist=0
    for i in range (0, len(utterances)):
        x=utterances[i].split("\t")
        if (x[1]=="INTENT"):
            if numlist>0:
#                json = json + ",\n"
                f.write(",\n")
            numlist = numlist + 1
#            json = json + "\t\t{\n"
#            json = json + "\t\t\t\"Name\": \"" + x[2].replace("\"","\\\"") + "\"\n"
#            json = json + "\t\t}"
            f.write("\t\t{\n")
            f.write("\t\t\t\"Name\": \"" + x[2].replace("\"","\\\"") + "\"\n")
            f.write("\t\t}")
            intent_name=x[2] #GNAPA!!!
#    json = json + "\n\t],\n"
#    json = json + "\t\"entities\": [\n"
    f.write("\n\t],\n")
    f.write("\t\"entities\": [\n")
    numlist=0
    for i in range (0, len(utterances)):
        x=utterances[i].split("\t")
        if (x[1]=="ENTITY"):
            if numlist>0:
#                json = json + ",\n"
                f.write(",\n")
            numlist = numlist + 1
#            json = json + "\t\t{\n"
#            json = json + "\t\t\t\"Name\": \"" + x[2].replace("\"","\\\"") + "\"\n"
#            json = json + "\t\t}"
            f.write("\t\t{\n")
            f.write("\t\t\t\"Name\": \"" + x[2].replace("\"","\\\"") + "\"\n")
            f.write("\t\t}")
#    json = json + "\n\t],\n"
#    json = json + "\t\"bing_entities\": [],\n"
#    json = json + "\t\"actions\": [],\n"
#    json = json + "\t\"model_features\": [],\n"
#    json = json + "\t\"regex_features\": [],\n"
#    json = json + "\t\"utterances\": [\n"
    f.write("\n\t],\n")
    f.write("\t\"bing_entities\": [],\n")
    f.write("\t\"actions\": [],\n")
    f.write("\t\"model_features\": [],\n")
    f.write("\t\"regex_features\": [],\n")
    f.write("\t\"utterances\": [\n")
    tag_regex=re.compile(":#[^#]+#:")
    numlist=0
    for i in range (0, len(utterances)):
        x=utterances[i].split("\t")
        if (x[1]!="ENTITY") and x[1]!="INTENT":
            if numlist>0:
#                json = json + ",\n"
                f.write(",\n")
            numlist = numlist + 1
#            json = json + "\t\t{\n"
#            json = json + "\t\t\t\"text\": \"" + tag_regex.sub("",x[2]).replace("\"","\\\"") + "\",\n"
#            json = json + "\t\t\t\"intent\": \"" + intent_name.replace("\"","\\\"") + "\",\n"
            f.write("\t\t{\n")
            f.write("\t\t\t\"text\": \"" + tag_regex.sub("",x[2]).replace("\"","\\\"") + "\",\n")
            f.write("\t\t\t\"intent\": \"" + intent_name.replace("\"","\\\"") + "\",\n")
            m=tag_regex.search(x[2])
            numlist2=0
            while m is not None:
                if numlist2==0:
#                    json = json + "\t\t\t\"entities\": [\n"
                    f.write("\t\t\t\"entities\": [\n")
                if numlist2>0:
#                    json = json + ",\n"
                    f.write(",\n")
                numlist2 = numlist2 +1
                start=m.start(0)
                tag=m.group(0)
                tag=tag.replace("#","")
                tag=tag.replace(":","")
                x[2] = tag_regex.sub("", x[2], 1)
                m=tag_regex.search(x[2])
                end=m.start(0)
                x[2] = tag_regex.sub("", x[2], 1)
                value = x[2][start:end]
                m=tag_regex.search(x[2])
#                json = json + "\t\t\t\t{\n"
#                json = json + "\t\t\t\t\t\"entity\": \"" + tag.replace("\"","\\\"") + "\",\n"
#                json = json + "\t\t\t\t\t\"_entity_text\": \"" + value.replace("\"","\\\"") + "\",\n"
#                json = json + "\t\t\t\t\t\"startPOS\": " + str(start) + ",\n"
#                json = json + "\t\t\t\t\t\"endPOS\": " + str(end-1) + "\n"
#                json = json + "\t\t\t\t}"
                f.write("\t\t\t\t{\n")
                f.write("\t\t\t\t\t\"entity\": \"" + tag.replace("\"","\\\"") + "\",\n")
                f.write("\t\t\t\t\t\"_entity_text\": \"" + value.replace("\"","\\\"") + "\",\n")
                f.write("\t\t\t\t\t\"startPOS\": " + str(start) + ",\n")
                f.write("\t\t\t\t\t\"endPOS\": " + str(end-1) + "\n")
                f.write("\t\t\t\t}")
#            json = json + "\n\t\t\t]\n"
#            json = json + "\t\t}"
            f.write("\n\t\t\t]\n")
            f.write("\t\t}")
#    json = json + "\n\t]\n"
#    json = json + "}\n"
    f.write("\n\t]\n")
    f.write("}\n")
#    return json
    return

def expand_variations_low(items, nitems, this_item, sentence, expansions):
    if (this_item==nitems):
        xsentence=sentence.replace("\t ","\t")
        if xsentence not in expansions:
            expansions.append(xsentence)
        return
    else:
        xl = items[this_item].split("|")
        for ixl in range (0, len(xl)):
            xsentence=sentence + " " + xl[ixl]
            expand_variations_low(items, nitems, this_item + 1, xsentence, expansions)
        return

def expand_variations(psentence):
    a1 = psentence.split("\t")
    tag = a1[0]
    auxtag = a1[1]
    items = a1[2]
    if re.search("[(][)]",items) is not None:
        expansions=[]
        return expansions
    regex=re.compile("[(][^(]*[)]")
    list=regex.findall(items)
    for i in range (0, len(list)):
        list[i]=list[i].replace("(","")
        list[i]=list[i].replace(")","")
        expansions = []
        sentence = tag + "\t" + auxtag + "\t"
        expand_variations_low(list, len(list), 0, sentence, expansions)
    return expansions

def generate_variations0(slot_info, f_frames_mapping, filedic1, filedic2, blocks, multi_blocks):
    btxlex.loadBtxLex()
    c_init_btx = btxlex.btxInitialize(4)
    init_btx = ctypes.c_void_p(c_init_btx).value
    if init_btx is not None:
        load1 = btxlex.btxLoadUsrDict(c_init_btx, filedic1, "TAGS", 1)
        if load1 == 0:
            btxlex.btxFinalize(c_init_btx)
            c_init_btx=0
            return []
        else:
            c_init_btx_2 = btxlex.btxInitialize(4)
            init_btx_2 = ctypes.c_void_p(c_init_btx_2).value
            if init_btx_2 is not None:
                load2 = btxlex.btxLoadUsrDict(c_init_btx_2, filedic2, "LEX", 1)
                if load2 == 0:
                    btxlex.btxFinalize(c_init_btx_2)
                    btxlex.btxFinalize(c_init_btx)
                    c_init_btx=0
                    c_init_btx_2=0
                    return []

    slot_cnt = len(slot_info)
    if "frame_id" in slot_info.keys():
        frame_info = int(slot_info["frame_id"])
        slot_cnt -= 1
    else:
        frame_info = int("3")
    if "polarity" in slot_info.keys():
        slot_cnt -= 1
    if "interrogative" in slot_info.keys():
        slot_cnt -= 1
        
##    print(slot_info)
    fids = []
    xs2 = []
    xs3 = []
    xmap = {}

    the_map = []

    templates = []
    intent_name = ""

### Look for the appropriate frame definition for NLG in FRAMES_mapping.txt
### FRAMES_mapping.txt. To be built by hand. Partially derived from 'ONTOLOGY.txt' and 'FRAMES.txt', as taken from the slots service
### One or more lines with the orderings for the frame templates follow the main frame template definition. These lines have the first field empty.
    iscomment = re.compile(r"^\s*;")
    with open(f_frames_mapping) as csvfile:
        read_csv = csv.reader(csvfile, delimiter='\t')
        for row in read_csv:
            if len(row)>=1:
                if iscomment.match(row[0]) is None:
                    if len(row) > 3 :
                        fid = int(row[0])
                        if fid == frame_info:
                            is_this_one = True
                            templates = []
                            intent_name = row[3]
                            formulae = row[4]
                            s2 = row[1]
                            s3 = row[2]
                            xs2 = s2.split()
                            xs3 = s3.split()
                            for i in range(0, len(xs2)):
                                xmap[xs2[i]] = xs3[i]
                        else:
                            is_this_one = False
                    else:
                        if is_this_one:
                            if (len(row)==2) or (len(row)==3):
                                if row[0][0:0]!=";":
                                    if row not in templates:
                                        templates.append(row)
                            if (len(row)==1):
                                if row[0][0:0]!=";":
                                    xmb=row[0].split("=")
                                    if len(xmb) == 2 :
                                        if xmb[0]=="MULTIBLOCK" :
                                            fxmb=int(xmb[1])
                                            for xmbrow in multi_blocks[fxmb]:
                                                if xmbrow not in templates:
                                                    templates.append(xmbrow)

##    print(templates)
### Extra entites coming from REGEXes and not explictly stated in the FRAME definition
    xmap["#HOUR#"] = "#Hour#"
    xmap["#PROGRAM#"] = "#Program#"
    xmap["#CHANNEL#"] = "#Channel#"
    xmap["#TIME#"] = "#Time#"
    xmap["#DEGREES#"] = "#Degrees#"
    xmap["#PERCENTAGE#"] = "#Percentage#"
    xmap["#PRE-FORMULA#"] = "#Pre-formula#"
    xmap["#POST-FORMULA#"] = "#Post-formula#"
    xmap["#FORMULA#"] = "#Formula#"
    xmap["#ENDING#"] = "#Ending#"

## Mapping fields entities.
### Just the entitites and entity types detected in the sample input are used.
### One map
### zdic entity-type => entity-value
    zdic = {}
    for x2 in slot_info.keys():
        ss = "#" + x2.upper() + "#"
        if ss in xmap.keys():
            ss1 = xmap[ss]
            xx1 = str(slot_info.get(x2)).replace(" ", "_")        #Gnapa due to REGEXes, v.g. "30 degrees"
            zdic[ss1] = xx1

### Generate base sentences from templates.
###   Templates with extra, non matched, segments are discarded (sentences not generated)
###   Templates with insufficient number of segments are discarded (sentences not generated)
    spcrem = re.compile(r"(^ +| +$)")
    ent = re.compile(r"#[^#]+#")
    tag_regex = re.compile(":#[^#]+#:")
    pre_sents = {}
    sents = []
    entities = []
    for ix in range(0, len(templates)):
        tag = templates[ix][0]
        flags = templates[ix][1]
        ss = templates[ix][2]
        q = ss.split(" + ")
        out0 = ""
        match_cnt = 0
        matches=""
#        print(">>>" + templates[ix][2])
        maxatts = 0
        for jx in range(0, len(q)):
            parts=q[jx].split(".")
            att_n=0
            str_grep=""
            atts_list={}
            const = ""
            for px in range (0, len(parts)):
                xx=parts[px].split("=")
                key = xx[0]
                val = xx[1]
                if key == "block" :
                    if val not in blocks.keys():
                        if jx==0:
                            ss2="const=" + val
                        else:
                            ss2="const=" + " + " + val
                    else:
                        if jx==0:
                            ss2=blocks[val]
                        else:
                            ss2=ss2 + " + " + blocks[val]
                    break
                else:
                    if jx==0:
                        ss2=q[jx]
                    else:
                        ss2=ss2 + " + " + q[jx]
                    break
        ss = ss2
        nents = ent.findall(ss)
        if len(nents) != slot_cnt:
            if ix in pre_sents:
                pre_sents[ix] = ""
            else:
                pre_sents[ix] = ""
            continue
        ss2 =""
        q = ss.split(" + ")
        for jx in range(0, len(q)):
            parts=q[jx].split(".")
            att_n=0
            str_grep=""
            atts_list={}
            const = ""
            for px in range (0, len(parts)):
                xx=parts[px].split("=")
                key = xx[0]
                val = xx[1]
                if key == "lemma" :
                    lst = val.split(",")
                    ll = "("
                    for llx in range (0, len(lst)):
                        if lst[llx] not in zdic.keys():
                            if (llx == 0):
                                ll = ll + lst[llx]
                            else:
                                ll = ll + "|" + lst[llx]
                        else:
                            match_cnt = match_cnt + 1
                            entity=lst[llx].replace("#","")
                            if entity not in entities:
                                entities.append(entity)
                            if (llx == 0):
                                ll = ll + ":" + lst[llx] + ":" + zdic[lst[llx]]
                            else:
                                ll = ll + "|" + ":" + lst[llx] + ":" + zdic[lst[llx]]
                    ll = ll + ")"
                else:
                    if key == "const" :
                        lst = val.split(",")
                        const = "("
                        for llx in range (0, len(lst)):
                            if lst[llx] not in zdic.keys():
                                if (llx == 0):
                                    const = const + lst[llx]
                                else:
                                    const = const + "|" + lst[llx]
                            else:
                                match_cnt = match_cnt + 1
                                if (llx == 0):
                                    const = const +  ":" + lst[llx] + ":" +zdic[lst[llx]]
                                else:
                                    const = const + "|" +  ":" + lst[llx] + ":" +zdic[lst[llx]]
                        const = const + ")"
                    else:
                        cp_list=ctypes.POINTER(ctypes.c_char_p)()
                        cp_list, l_list = btxlex.btxRootToForm(c_init_btx, key)
                        for  i in range (0, l_list):
                            nx=cp_list[i]
                            the_form = nx.decode("utf-8")
                            lst = val.split(",")
                            xs = "^("
                            for llx in range (0, len(lst)):
                                if (llx == 0):
                                    xs = xs + "(" + lst[llx] + "|[^\t]+," + lst[llx] + "|" + lst[llx] + ",[^\t]+)"
                                else:
                                    xs = xs + "|" + "(" + lst[llx] + "|[^\t]+," + lst[llx] + "|" + lst[llx] + ",[^\t]+)"
                            xs = xs + ")"
                            m=re.search(xs, the_form)
                            if m is not None:
                                cp_listf = ctypes.POINTER(ctypes.c_char_p)()
                                cp_listf, l_listf = btxlex.btxFormToRoot(c_init_btx, the_form)
                                for  j in range (0, l_listf):
                                    nx2 = cp_listf[j]
                                    s2 = nx2.decode("utf-8")
                                    qatt = s2.split("#")
                                    natt = int(qatt[1])
                                    if natt not in atts_list.keys():
                                        atts_list[natt] = qatt[2]
                                    else:
                                        atts_list[natt] = atts_list[natt] + "|" + qatt[2]
                                    if natt > maxatts :
                                        maxatts = natt
                                if l_listf > 0:
                                    btxlex.btxFreeWordList(cp_listf,l_listf)
                        if l_list > 0:
                            btxlex.btxFreeWordList(cp_list,l_list)
            if is_not_blank(const):
                s = "(" + const.replace("_"," ") + ")"
            else:
                ml = tag_regex.search(ll)
                if ml is not None:
                    ll_clean = tag_regex.sub("", ll)
                    tag=ml.group(0)
                else:
                    ll_clean = ll
                    tag=""
                s = ll_clean + "[+]M"
                for ixx in range (3, maxatts + 1):
                    if ixx not in atts_list.keys():
                        s = s + "#[0-9]+"
                    else:
                        s = s + "#(" + atts_list[ixx] + ")"
                s = s + "#"
            if ix in pre_sents:
                pre_sents[ix] = pre_sents[ix] + " + " + tag + s
            else:
                pre_sents[ix] = tag + s
        if match_cnt != slot_cnt:
            if ix in pre_sents:
                pre_sents[ix] = ""
            else:
                pre_sents[ix] = ""
            continue
        for iis in range (0, len(pre_sents)):
            if pre_sents[iis] == "":
                continue
            ss = templates[iis][0] + "\t" + templates[iis][1] + "\t"
            nss = 0
            ssx = pre_sents[iis].split(" + ")
            for jjs in range (0, len(ssx)):
                m = re.search("[[][+][]]M", ssx[jjs])
                if m is not None:
                    lll = ssx[jjs].split("[+]M")
                    lemma_list=lll[0]
                    lemma_list=lemma_list.replace("(","")
                    lemma_list=lemma_list.replace(")","")
                    ml = tag_regex.search(lemma_list)
                    if ml is not None:
                        lemma_list = tag_regex.sub("", lemma_list)
                        tag=ml.group(0)
                    else:
                        tag=""
                    l2r=lemma_list.split("|")
                    item = "("
                    nitem = 0
                    for lxi in range (0, len(l2r)):
                        cp_listX =ctypes.POINTER(ctypes.c_char_p)()
                        my_form = l2r[lxi].replace("_"," ")
                        cp_listX, l_listX = btxlex.btxRootToForm(c_init_btx_2, my_form)
                        pre_root=""
                        post_root=""
                        root=""
                        regex=ssx[jjs]
                        regex=tag_regex.sub("",regex)
                        regex=regex.replace("_"," ")
#GNAPA TO BE REMOVED
                        if l_listX == 0:
                            multix=l2r[lxi].split("_")
                            root=multix[len(multix)-1]
                            regex=ssx[jjs]
                            regex=tag_regex.sub("",regex)
                            regex=regex.replace(l2r[lxi],root)
                            post_root=""
                            pre_root=multix[0] + " "
                            for imx in range(2, len(multix)):
                                pre_root=pre_root + " " + multix[imx]
                            cp_listX =ctypes.POINTER(ctypes.c_char_p)()
                            cp_listX, l_listX = btxlex.btxRootToForm(c_init_btx_2, root)
                            if l_listX <= 1:
                                multix=l2r[lxi].split("_")
                                root=multix[0]
                                regex=ssx[jjs]
                                regex=tag_regex.sub("",regex)
                                regex=regex.replace(l2r[lxi],root)
                                pre_root=""
                                if (len(multix)>1):
                                    post_root=" " + multix[1]
                                    for imx in range(2, len(multix)):
                                        post_root=post_root+" "+multix[imx]
                                else:
                                    post_root=""
                                cp_listX =ctypes.POINTER(ctypes.c_char_p)()
                                cp_listX, l_listX = btxlex.btxRootToForm(c_init_btx_2, root)
#END OF GNAPA TO BE REMOVED
                        for it in range (0, l_listX):
                            txt=cp_listX[it].decode("utf-8")
                            cp_listY =ctypes.POINTER(ctypes.c_char_p)()
                            cp_listY, l_listY = btxlex.btxFormToRoot(c_init_btx_2, txt)
                            for itY in range (0, l_listY):
                                txtY=cp_listY[itY].decode("utf-8")
                                m=re.search(regex, txtY)
                                if m is not None:
                                    qatt = txt.split("\t")
                                    if nitem == 0 :
                                        item = item + tag + pre_root + qatt[0] + post_root + tag
                                        nitem = nitem + 1
                                    else:
                                        item = item + "|" + tag + pre_root + qatt[0] + post_root + tag
                            if l_listY > 0:
                                btxlex.btxFreeWordList(cp_listY,l_listY)
                        if l_listX > 0:
                            btxlex.btxFreeWordList(cp_listX,l_listX)
                    item = item + ")"
                else:
                    item = ssx[jjs]
                if nss == 0 :
                    ss = ss + item
                    nss = nss + 1
                else:
                    ss = ss + " " + item
##            print(">>>"+ss)
            vx = expand_variations(ss)
            for ivx in range (0, len(vx)):
                if (0 == 1):
                    m=re.search(r"(\ban [^aeiouAEIOU]|\ba [aeiouAEIOU])",vx[ivx])
                    if m is None:
                        vx[ivx]=re.sub("^, ","", vx[ivx])
                        vx[ivx]=vx[ivx].replace(" '","'")
                        vx[ivx]=vx[ivx].replace(" ,",",")
                        vx[ivx]=vx[ivx].replace(" ;",";")
                        vx[ivx]=vx[ivx].replace(" .",".")
                        vx[ivx]=vx[ivx].replace(" ?","?")
                        vx[ivx]=vx[ivx].replace(" !","!")
                        vx[ivx]=vx[ivx].replace("¿ ","¿")
                        vx[ivx]=vx[ivx].replace("¡ ","¡")
                        vx[ivx]=vx[ivx].replace("s's","s'")
                        vx[ivx]=spcrem.sub("",vx[ivx])
                else:
                    if vx[ivx] not in sents:
                         sents.append(vx[ivx])
    btxlex.btxFinalize(c_init_btx)
    btxlex.btxFinalize(c_init_btx_2)
    if frame_info >=0:
        for i in range (0, len(entities)):
            out="ENTITY\t0_0\t"+entities[i]
            sents.insert(0, out)
        out="INTENT\t0_0\t"+intent_name
        sents.insert(0, out)
    entities=[]
    return sents

def combinate (tag_sent, tag_prev, tag_post):
    xsent=tag_sent.split("_")
    xprev=tag_prev.split("_")
    xpost=tag_post.split("_")
    level=int(xsent[1])+(int(xprev[1])*int(xpost[1]))
    tags=xsent[0]+xprev[0]+xpost[0]
    final_tag=""
    for tag in SENTENCE_TYPES_LIST:
        if tag in tags:
            final_tag=final_tag+tag
    return final_tag+"_"+str(level)

def generate_variations_without_formulae(slot_info, f_frames_mapping, filedic1, filedic2, out_base_sentences, flags):
    iscomment = re.compile(r"^\s*;")
    multi_blocks={}
    with open(f_frames_mapping) as csvfile:
        read_csv = csv.reader(csvfile, delimiter='\t')
        for row in read_csv:
            if len(row)>=1:
                if iscomment.match(row[0]) is None:
                    if len(row) > 3 :
                        fid = int(row[0])
                        if fid < -100:
                            is_this_one = True
                            if fid not in multi_blocks.keys():
                                multi_blocks[fid]=[]
                        else:
                            is_this_one = False
                    else:
                        if is_this_one:
                            if row not in multi_blocks[fid]:
                                multi_blocks[fid].append(row)
    blocks={}
    with open(f_frames_mapping) as csvfile:
        read_csv = csv.reader(csvfile, delimiter='\t')
        for row in read_csv:
            if len(row)>=1:
                if iscomment.match(row[0]) is None:
                    if len(row) > 3 :
                        fid = int(row[0])
                        if fid == -10:
                            is_this_one = True
                            raw_blocks = []
                        else:
                            is_this_one = False
                    else:
                        if is_this_one:
                            if row[0] not in blocks.keys():
                                blocks[row[0]]=row[2]
    sents=[]
    sents = generate_variations0(slot_info, f_frames_mapping, filedic1, filedic2, blocks, multi_blocks)
### Select according flags
    if flags=="*":
        sents2=[]
        for sent in sents:
            ms = sent.split("\t")
            type_eval = combinate(ms[1],"0_1","0_1")
            mx = type_eval.split("_")
            xsent="{0:0>9}".format(mx[1])+"_"+mx[0]+"\t"+ms[0]+"\t"+ms[2]
            sents2.append(xsent)
        sents=[]
        sents2.sort()
        return sents2
    else:
        mf = flags.split("_")
        mrex = re.compile(mf[0])
        mrex2 = re.compile("^(ENTITY|INTENT)$")
    sents2=[]
    for sent in sents:
        ms = sent.split("\t")
        type_eval = combinate(ms[1],"0_1","0_1")
        mx = type_eval.split("_")
        k = mrex.search(mx[0])
        k2 = mrex2.search(ms[0])
        if k2 is None:
            if k is not None:
                if (int(mx[1])<=int(mf[1])):
                    xsent="{0:0>9}".format(mx[1])+"_"+mx[0]+"\t"+ms[0]+"\t"+ms[2]
                    sents2.append(xsent)
        else:
            xsent="{0:0>9}".format(mx[1])+"_"+mx[0]+"\t"+ms[0]+"\t"+ms[2]
            sents2.append(xsent)
    sents=[]
    sents2.sort()
    return sents2

def generate_variations_with_formulae(slot_info, f_frames_mapping, filedic1, filedic2, out_base_sentences, flags):
    iscomment = re.compile(r"^\s*;")
    spcrem = re.compile(r"(^\s+|\s+$)")
    tabspcrem = re.compile(r"(\t\s+|\s+\t)")
    force_single=out_base_sentences
    multi_blocks={}
    with open(f_frames_mapping) as csvfile:
        read_csv = csv.reader(csvfile, delimiter='\t')
        for row in read_csv:
            if len(row)>=1:
                if iscomment.match(row[0]) is None:
                    if len(row) > 3 :
                        fid = int(row[0])
                        if fid < -100:
                            is_this_one = True
                            if fid not in multi_blocks.keys():
                                multi_blocks[fid]=[]
                            nmb = 0
                        else:
                            is_this_one = False
                    else:
                        if is_this_one:
                            if row not in multi_blocks[fid]:
                                multi_blocks[fid].append(row)
    blocks={}
    with open(f_frames_mapping) as csvfile:
        read_csv = csv.reader(csvfile, delimiter='\t')
        for row in read_csv:
            if len(row)>=1:
                if iscomment.match(row[0]) is None:
                    if len(row) > 3 :
                        fid = int(row[0])
                        if fid == -10:
                            is_this_one = True
                            raw_blocks = []
                        else:
                            is_this_one = False
                    else:
                        if is_this_one:
                            if row[0] not in blocks.keys():
                                blocks[row[0]]=row[2]
    sents=[]
    sents = generate_variations0(slot_info, f_frames_mapping, filedic1, filedic2, blocks, multi_blocks)
###    print(len(sents))
### FORMULAE COMPOSITION COMBINATIONS
    final = []
    qqq = "{\"frame_id\": \"-1\", \"polarity\": \"affirmative\"}"
    jqqq = json.loads(qqq)
    previous = generate_variations0(jqqq, f_frames_mapping, filedic1, filedic2, blocks, multi_blocks)
##    print(previous)
    qqq = "{\"frame_id\": \"-2\", \"polarity\": \"affirmative\"}"
    jqqq = json.loads(qqq)
    posterior = generate_variations0(jqqq, f_frames_mapping, filedic1, filedic2, blocks, multi_blocks)
##    print(posterior)
    for i1 in range (0, len(sents)):
##        printf("%s\r",str(i1))
        x1=sents[i1].split("\t")
        if (x1[0]!="ENTITY") and x1[0]!="INTENT":
            if force_single:
                type_val=combinate(x1[1], "0_1", "0_1")
                combination=x1[0] + "\t" + type_val + "\t" + x1[2]
                combination=combination.replace("··COMMA··",",")
                combination=re.sub("^, ","",combination)
                combination=combination.replace(" '","'")
                combination=combination.replace(" ,",",")
                combination=combination.replace(" ;",";")
                combination=combination.replace(" .",".")
                combination=combination.replace(" ?","?")
                combination=combination.replace(" !","!")
                combination=combination.replace("¿ ","¿")
                combination=combination.replace("¡ ","¡")
                combination=combination.replace("s's","s'")
                combination=spcrem.sub("",combination)
                if combination not in final:
                    final.append(combination)
            for i2 in range (0, len(previous)):
                x2=previous[i2].split("\t")
                rxitems=x1[0].replace(",","|")
                regex="(^|,)("+rxitems+")(,|$)"
                m=re.search(regex,x2[0])
                if m is not None:
                    for i3 in range (0, len(posterior)):
                        x3=posterior[i3].split("\t")
                        m3=re.search(regex,x3[0])
                        if m3 is not None:
##                            print("!!!"+x2[0]+"\t"+x2[1]+"\t"+x2[2])
##                            print("!!!"+x3[0]+"\t"+x3[1]+"\t"+x3[2])
                            y2=x2[1].split(":")
                            y3=x3[1].split(":")
                            if (y2[1]==y3[1]):
                                type_val=combinate(x1[1], y2[0], y3[0])
                                combination=x1[0] + "\t" + type_val + "\t" + x2[2] + " " + x1[2] + " " + x3[2]
                                combination=combination.replace("··COMMA··",",")
                                combination=re.sub("^, ","",combination)
                                combination=combination.replace(" '","'")
                                combination=combination.replace(" ,",",")
                                combination=combination.replace(" ;",";")
                                combination=combination.replace(" .",".")
                                combination=combination.replace(" ?","?")
                                combination=combination.replace(" !","!")
                                combination=combination.replace("¿ ","¿")
                                combination=combination.replace("¡ ","¡")
                                combination=combination.replace("s's","s'")
                                combination=spcrem.sub("",combination)
                                combination=tabspcrem.sub("\t",combination)
                                if combination not in final:
                                    final.append(combination)
        else:
            if sents[i1] not in final:
                final.append(sents[i1])
    sents=[]
    previous=[]
    posterior=[]
### Select according flags
    if flags=="*":
        sents2=[]
        for sent in final:
            ms = sent.split("\t")
            mx = ms[1].split("_")
            xsent="{0:0>9}".format(mx[1])+"_"+mx[0]+"\t"+ms[0]+"\t"+ms[2]
            sents2.append(xsent)
        final=[]
        sents2.sort()
        return sents2
    else:
        mf = flags.split("_")
        mrex = re.compile(mf[0])
        mrex2 = re.compile("^(ENTITY|INTENT)$")
    sents2=[]
    for sent in final:
        ms = sent.split("\t")
        mx = ms[1].split("_")
        k = mrex.search(mx[0])
        k2 = mrex2.search(ms[0])
        if k2 is None:
            if k is not None:
                if (int(mx[1])<=int(mf[1])):
                    xsent="{0:0>9}".format(mx[1])+"_"+mx[0]+"\t"+ms[0]+"\t"+ms[2]
                    sents2.append(xsent)
        else:
            xsent="{0:0>9}".format(mx[1])+"_"+mx[0]+"\t"+ms[0]+"\t"+ms[2]
            sents2.append(xsent)
    final=[]
    sents2.sort()
    return sents2

def generate_variations(slot_info, f_frames_mapping, filedic1, filedic2, use_formulae, out_base_sentences, flags):
    if use_formulae :
        x=generate_variations_with_formulae(slot_info, f_frames_mapping, filedic1, filedic2, out_base_sentences, flags)
        return x
    else :
        x=generate_variations_without_formulae(slot_info, f_frames_mapping, filedic1, filedic2, out_base_sentences, flags)
        return x
