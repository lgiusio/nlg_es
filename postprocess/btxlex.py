import os
import sys
import ctypes

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

def loadBtxLex():
### 'frame_info' is to contain the template_frame id number. Entry point to 'FRAMES_mapping.txt' below
##LINUX     hllDll = ctypes.CDLL(os.path.join(PROJECT_ROOT, 'BtxLex'), mode=ctypes.RTLD_GLOBAL)
    hllDll = ctypes.CDLL(os.path.join(PROJECT_ROOT, 'BtxLex.dll'), mode=ctypes.RTLD_GLOBAL)

    global btx_Initialize
    global btx_Finalize
    global btx_LoadUsrDict
    global btx_FreeWordList
    global btx_RootToForm
    global btx_FormToRoot

    btx_Initialize = hllDll.btx_Initialize
    btx_Initialize.restype = ctypes.c_void_p
    btx_Initialize.argtypes = [ctypes.c_ulong]

    btx_Finalize = hllDll.btx_Finalize
    btx_Finalize.restype = None
    btx_Finalize.argtypes = [ctypes.c_void_p]

    btx_LoadUsrDict = hllDll.btx_LoadUsrDict
    btx_LoadUsrDict.restype = ctypes.c_long
    btx_LoadUsrDict.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_char_p, ctypes.c_ulong]

    btx_FreeWordList = hllDll.btx_FreeWordList
    btx_FreeWordList.restype = None
    btx_FreeWordList.argtypes = [ctypes.POINTER(ctypes.c_char_p), ctypes.c_long]

    btx_RootToForm = hllDll.btx_RootToForm
    btx_RootToForm.restype = ctypes.POINTER(ctypes.c_char_p)
    btx_RootToForm.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.POINTER(ctypes.c_long)]

    btx_FormToRoot = hllDll.btx_FormToRoot
    btx_FormToRoot.restype = ctypes.POINTER(ctypes.c_char_p)
    btx_FormToRoot.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.POINTER(ctypes.c_long)]

def btxInitialize(flags):
    p1=ctypes.c_ulong(flags)
    c_init_btx = btx_Initialize(p1)
    return c_init_btx

def btxFinalize(ptr):
    btx_Finalize(ptr)
    return

def btxLoadUsrDict(ptr, fname, symname, flags):
    filedic1 = os.path.join(".\\", fname)
    p2 = ctypes.c_char_p(filedic1.encode("utf-8"))
    p3 = ctypes.c_char_p(bytes(symname,"utf-8"))
    p4 = ctypes.c_ulong(flags)
    c_load1 = btx_LoadUsrDict(ptr, p2, p3, p4)
    load1 = ctypes.c_long(c_load1).value
    return load1

def btxRootToForm(ptr, word):
    p2 = ctypes.c_char_p(bytes(word,"utf-8"))
    p3 = ctypes.c_long(0)
    cp_list=ctypes.POINTER(ctypes.c_char_p)()
    cp_list = btx_RootToForm(ptr, p2, ctypes.byref(p3))
    num_forms=p3.value
    return cp_list, num_forms

def btxFormToRoot(ptr, word):
    p2a = ctypes.c_char_p(bytes(word,"utf-8"))
    p3a = ctypes.c_long(0)
    cp_listf = ctypes.POINTER(ctypes.c_char_p)()
    cp_listf = btx_FormToRoot(ptr, p2a, ctypes.byref(p3a))
    l_listf=p3a.value
    return cp_listf, l_listf

def btxFreeWordList(cp_list, num_items):
    p2 = ctypes.c_long(num_items)
    btx_FreeWordList(cp_list, p2)
    return
