#!/usr/bin/python3

import requests, json
import re
import csv
import sys, os
import postprocess.variations_generator as variations_generator

## MAIN

## CALL TO NLG_SLOTS SERVICE
oauth_token = '5f473c886e924543aaaa015c4a30f8bb'
##    endpoint = "http://88.99.241.175:4001/nlgslots/"
endpoint = "https://beta.api.bitext.com:4001/nlgslots/"
headers = { "Authorization" : "bearer " + oauth_token, "Content-Type" : "application/json" }
for sentence in open("NLG_SPA_RT_corpus.txt"):
    ###### !!! 'language' and 'text' should enter as params to the python script as part of the service query
    sentence = sentence.rstrip('\n')
    sentence = sentence.rstrip('\r')
    print ("==> " + sentence)
    params = {"language" : "spa","text" : sentence, "simple" : True}

    res = requests.post(endpoint, headers=headers, data=json.dumps(params))

    resultid = json.loads(res.text).get('resultid')
    
    
    analysis = None
    
    while analysis == None:

        res = requests.get(endpoint + resultid + '/', headers=headers)
        if res.status_code == 200 :
            analysis = res.text
    the_json=res.text
    print (the_json)
    x_info = json.loads(the_json)

    frames_file=os.path.join(os.getcwd(), "ontologies/FRAMES_NLG_SPA_LOR_20180129A_obj.txt")
    x_file="ontologies/X.txt"
    x2_file="ontologies/X2.txt"

    ## 'frame_info' is to contain the template_frame id number. Entry point to 'FRAMES_mapping.txt' below
    if "frame_id" in x_info["slotsanalysis"][0].keys():
        frame_info=int(x_info["slotsanalysis"][0]["frame_id"])
        slot_cnt=len(x_info["slotsanalysis"][0])-2
    else:
        frame_info=int("0")
        slot_cnt=len(x_info["slotsanalysis"][0])

    flags="^(B|BM|BMS)$_20"
    flags="[^S]_20"
    flags="*"
    flags="^B[^M]+$_4"
    flags="^[BIS]+$_4"
    flags="^B[^M]+$_20"
    flags="*"

    #### GENERATION OF RAW SENTENCES
    #### 3rd parameter to True means: "include formulae".
    #### 3rd parameter to False means: "DO NOT include formulae".
    #### 4th parameter to True means: "include base sentences in output".
    #### 4th parameter to False means: "DO NOT include base sentences in output".
    #### 5th parameter flags: Controls varios output options. (Still to be developed).

    #### without APPLYING FORMULAE
    ####results = variations_generator.generate_variations(x_info["slotsanalysis"][0], frames_file, x_file, x2_file, False, True, flags)

    #### APPLYING FORMULAE
    results = variations_generator.generate_variations(x_info["slotsanalysis"][0], frames_file, x_file, x2_file, True, False, flags)
    if 0 == 1 :
        for result in results:
            print(result)

    ### UNTAGGED SENTENCES STR
    if 1 == 1 :
        outplain=variations_generator.string_list_to_plain_list_str(results)
        print(outplain)

    ### UNTAGGED SENTENCES STR
    if 0 == 1 :
        outplain=variations_generator.string_list_to_plain_list_str_with_label(results)
        print(outplain)

    ### TAGGED SENTENCES (MS_LUIS) STR
    if 0 == 1 :
        botname="I1_Auto"
        botdescription="\"Simplified\" version of the \"Home Autmation BOT\" for automated NL generation tests"
        botculture="en-us"
        outstr = variations_generator.string_list_to_MS_LUIS_str(botname, botdescription, botculture, results)
        print(outstr)


    ### OUTPUT TO FILES...
    outfname="open_door"

    ### UNTAGGED SENTENCES FILES
    if 0 == 1 :
        f = open(outfname + "_untagged.txt", "w")
        #####outplain = variations_generator.string_list_to_plain_list_file(results)
        variations_generator.string_list_to_plain_list_file(results, f)
        f.close()

    ### TAGGED SENTENCES (MS_LUIS) FILES
    if 0 == 1 :
        botname="I1_Auto"
        botdescription="\"Simplified\" version of the \"Home Autmation BOT\" for automated NL generation tests"
        botculture="en-us"
        f = open(outfname + "_tagged.txt", "w")
        variations_generator.string_list_to_MS_LUIS_file(botname, botdescription, botculture, results, f)
        f.close()
