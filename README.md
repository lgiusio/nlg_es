# NLG_Es

NLG project for Spanish

To run the code and generate sentences:

- open CyGwin
- in > ontologies > digit  sh compiler.sh FRAMES_201...(name of the file to run)
- open another window in CyGwin
- in the folder NLG_Es > digit > python3 caller.py

To work on the Rules of the FRAMES_Es

Structure of the file of FRAMES_Es related with the FRAMES (NLU)

The file is composed of 4 parts:

- FRAMES
- FORMULA
- BLOCK
- MULTIBLOCK

----- FRAMES:-----

This part is strictly related with the FRAMES file of NLU. In this part are stored all the informations about the number of the FRAMES to process and
the ELEMENTS that are part of the FRAMES and that, combine, allow to generate sentences.

Ex:

        0	                  #ACTION# #MEASURE# #OBJECT# #PLACE# #COLOR#	      #Action# #Measure# #Object# #Place# #Color#	    Action_Measure_Object_Place_Color_imperative imperative
- number of the FRAME (nlu)   list of the ELEMENTS (as in the FRAMES NLU)             the same list of ELEMENTS                                    Noun of the pattern

Above this title are insert all the MULTIBLOCK that are possible to use with the FRAME

Ex: 
;ACTION-OBJECT_object
MULTIBLOCK=-101
;ACTION-OBJECT_object [TIME] [HOUR]
MULTIBLOCK=-102
;ACTION-OBJECT_object-PLACE
MULTIBLOCK=-103
MULTIBLOCK=-104   

----- FORMULA -----

The "FORMULA" is the affix that it's possible adding to a BASIC sentence.

Ex:

- AFFIX => I want to    BASIC sentence => turn on the light => I want to turn on the light
- BASIC sentence => open the door      AFFIX =>, please     => open the door, please

To create a rule that allow to generate FORMULA, it's necessary have two fields, one for the prefix and one for the suffix with the same ID.

Prefix ==> FORMULA_start

- rule, rule, rule, rule, rule......	B_0:1A	const=
- rule, rule, rule, rule, rule......	BI_10:1C	const=do you mind
- rule, rule, rule, rule, rule......	BP_2:1B	const= 

Suffix ==> FORMULA_end

- rule, rule, rule, rule, rule......	B_0:1A	const= 
- rule, rule, rule, rule, rule......	BP_1:1B	const=··COMMA·· please
- rule, rule, rule, rule, rule......	BI_1:1C	const=?


Important!!!! All the rules, also that one that create a BASIC sentence, need to be added as prefix and suffix (see the number: B_0:1A in start need to be also in end)

-----NAME and NUMBER in the FORMULA------

To allow the users to filter the sentences it's necessary add a Tag and a number to each rule (in the formula and in the ID of the rule)
In the FORMULA -> B_0:1A
                  B = base sentence (other tags -> I=interrogative, P=polite, S=sintactic change M=morphological change)
				      It's possible add all the tags that we need adding the tag in the variations generation.py file
				  0 = the complexity of the sentence (0 is the no complex sentence)
				  1A = the ID for the rule in the FORMULA (need to be the same in the start and in the end)